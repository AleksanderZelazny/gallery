import HtmlWebpackPlugin from 'html-webpack-plugin';

export default {
  entry: [
    'babel-polyfill',
    './client/src/index.js'
  ],
  output: {
    path: '/',
    filename: 'bundle.js',
  },
  mode: 'none',
  module: {
    rules: [{
      use: 'babel-loader',
      test: /\.js$/,
      exclude: /node_modules/,
    },
    {
      use: ['style-loader', 'css-loader'],
      test: /\.css$/,
    }],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'client/public/index.html',
      inject: false
    })],
};
