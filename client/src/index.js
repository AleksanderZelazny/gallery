import 'raf/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import es6promise from 'es6-promise';
/* eslint-disable no-unused-vars */
import isomorphicFetch from 'isomorphic-fetch';
/* eslint-enable no-unused-vars */

import './style/style.css';

import Gallery from './components/gallery/gallery';

es6promise.polyfill();

const App = () => <Gallery />;

ReactDOM.render(
  <App />,
  document.querySelector('#app')
);
