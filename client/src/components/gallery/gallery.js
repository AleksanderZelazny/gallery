import React from 'react';
import Image from './image';

const updateImages = result => prevState => ({
  images: [...prevState.images, ...result.photo],
  page: result.page
});

const createGalleryUrl = page => `http://localhost:5000/gallery/all?page=${page}`;

class Gallery extends React.Component {
  constructor(props) {
    super(props);
    this.isLoading = false;
    this.state = {
      images: [],
      page: 0,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll.bind(this), false);
    this.loadMoreImages();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll.bind(this), false);
  }

  onScroll() {
    if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 500) &&
     !this.isLoading) {
      this.isLoading = true;
      this.loadMoreImages();
    }
  }

  loadMoreImages() {
    fetch(createGalleryUrl(this.state.page + 1))
      .then(response => response.json()).then((results) => {
        this.setState(updateImages(results));
        this.isLoading = false;
      });
  }

  filterValuesBasedOnColumn() {
    const columnImages = [[], [], [], []];
    let image;
    this.state.images.forEach((img, index) => {
      image = <Image key={img.id} image={img} />;
      columnImages[index % 4].push(image);
    });
    return columnImages;
  }

  render() {
    const columnImages = this.filterValuesBasedOnColumn();
    return (
      <div className="gallery-container">
        {this.state.images.length > 0 &&
          columnImages.map((images, index) =>
            <div key={index} className="gallery-column">{images}</div>)}
      </div>);
  }
}

export default Gallery;
