import React from 'react';
import PropTypes from 'prop-types';

const createFindImageInfoUrl = imageId =>
  `http://localhost:5000/gallery/info?imageId=${imageId}`;

class ImageDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      author: '',
      taken: '',
      originalFormat: '',
      originalUrl: ''
    };
  }
  componentDidMount() {
    fetch(createFindImageInfoUrl(this.props.image.id))
      .then(response => response.json()).then((results) => {
        this.setState({
          title: results.title._content,
          author: results.owner.username,
          taken: results.dates.taken,
          originalFormat: results.originalformat,
          originalUrl: results.urls.url[0]._content
        });
      });
  }
  render() {
    return (
      <div className="image-details">
        <ul className="image-details-list">
          <li><b>Title</b> : {this.state.title}</li>
          <li><b>Author</b> :
           <a href={this.state.originalUrl} target="_blank"> {this.state.author}</a>
          </li>
          <li><b>Taken</b> : {this.state.taken}</li>
          <li><b>Orginal Format</b> : {this.state.originalFormat}</li>
        </ul>
      </div>
    );
  }
}

ImageDetails.propTypes = {
  image: PropTypes.object.isRequired
};

export default ImageDetails;
