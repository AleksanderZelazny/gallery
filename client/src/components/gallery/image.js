import React from 'react';
import PropTypes from 'prop-types';
import ImageLightbox from './imageLightbox';


const createImageUrl = image =>
  `https://farm${image.farm}.staticflickr.com/${image.server}/${image.id}_${image.secret}.jpg`;

class Image extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.onClose = this.onClose.bind(this);
    this.state = {
      isOpen: false
    };
    this.url = createImageUrl(this.props.image);
  }

  onClick() {
    document.body.style.overflow = 'hidden';
    this.setState({ isOpen: true });
  }

  onClose() {
    document.body.style.overflow = 'visible';
    this.setState({ isOpen: false });
  }

  render() {
    const image = this.props.image;

    return (
      <div className="gallery-element">
        <img
          className="gallery-image"
          src={this.url}
          alt={image.title}
        />
        <span className="image-overlay" onClick={this.onClick}>
          <h3 className="overlay-text">{image.title}</h3>
        </span>
        {this.state.isOpen &&
        <ImageLightbox
          image={image}
          imageUrl={this.url}
          onCloseRequest={this.onClose}
        />}
      </div>
    );
  }
}

Image.propTypes = {
  image: PropTypes.object.isRequired
};

export default Image;
