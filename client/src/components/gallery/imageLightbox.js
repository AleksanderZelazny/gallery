import React from 'react';
import PropTypes from 'prop-types';
import ImageDetails from './imageDetails';

class ImageLightbox extends React.Component {
  constructor(props) {
    super(props);
    this.styleBackground = {
      backgroundImage: `url(${this.props.imageUrl})` // workaround to object-fit on IE
    };
  }

  componentDidMount() {
    document.body.style.overflow = 'hidden';
    document.addEventListener('keydown', this.escFunction.bind(this), false);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.escFunction.bind(this), false);
  }

  escFunction(event) {
    if (event.keyCode === 27) {
      this.props.onCloseRequest();
    }
  }

  render() {
    return (
      <div className="lightbox">
        <div className="lightbox-image" style={this.styleBackground} />
        <ImageDetails image={this.props.image} />;
      </div>);
  }
}

ImageLightbox.propTypes = {
  onCloseRequest: PropTypes.func.isRequired,
  imageUrl: PropTypes.string.isRequired,
  image: PropTypes.object.isRequired
};

export default ImageLightbox;
