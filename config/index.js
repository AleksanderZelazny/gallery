import server from './server';
import flickr from './flickr';

export default Object.assign({}, server, flickr);
