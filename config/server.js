const port = 5000;

const config = {
  server: {
    port
  }
};

export default config;
