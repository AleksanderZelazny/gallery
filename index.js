import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import express from 'express';
import webpackConfig from './webpack.config';
import config from './config/index';
import applicationRouter from './server/routes/index';

const app = express();

app.use(webpackMiddleware(webpack(webpackConfig)));
app.use('/', applicationRouter);

app.listen(config.server.port, () => {
  console.log(`Running on port ${config.server.port}`);
});
