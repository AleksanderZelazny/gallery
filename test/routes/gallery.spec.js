/* global it, describe */
import request from 'request';
import { expect } from 'chai';

const galleryUrl = 'http://localhost:5000/gallery/';

describe('Gallery Search Api', () => {
  const pageNumber = 1;
  const perPage = 16;
  const url = `${galleryUrl}all?page=${pageNumber}`;
  it('Search Api', (done) => {
    request(url, (error, response, body) => {
      const jsonBody = JSON.parse(body);
      expect(response.statusCode).to.equal(200);
      expect(jsonBody.page).to.equal(pageNumber);
      expect(jsonBody.perpage).to.equal(perPage);
      done();
    });
  });
});

describe('Info Api', () => {
  const imageId = 42041767211;
  const url = `${galleryUrl}info?imageId=${imageId}`;
  it('Info Api', (done) => {
    request(url, (error, response, body) => {
      const jsonBody = JSON.parse(body);
      expect(response.statusCode).to.equal(200);
      expect(jsonBody.originalformat).to.equal('jpg');
      expect(jsonBody.dateuploaded).to.equal('1526058437');
      done();
    });
  });
});
