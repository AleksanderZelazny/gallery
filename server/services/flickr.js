import Flickr from 'flickr-sdk';
import config from '../../config/index';

const apiKey = config.flickrConfig.apiKey;
const flickr = new Flickr(apiKey);


/**
 *  search description
 *
 *  @Param pageNumber {number} number of Page for which images are searched
 *  @return {list} list of images for phrase Barcelona on given page
 */
const search = pageNumber =>
  flickr.photos.search({
    text: 'barcelona',
    page: pageNumber,
    per_page: 16,
  }).then(res => res.body.photos);


  /**
   *  findPhotoInfo description
   *
   *  @Param imageId {number} id of image for which info is searched
   *  @return {object} informations about image with given id
   */
const findPhotoInfo = imageId =>
  flickr.photos.getInfo({
    api_key: apiKey,
    photo_id: imageId
  }).then(res => res.body.photo);


export default {
  search,
  findPhotoInfo
};
