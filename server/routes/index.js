import router from 'express';
import galleryRouter from './gallery';

const applicationRouter = router.Router();

applicationRouter.use('/gallery', galleryRouter);

export default applicationRouter;
