import router from 'express';
import asyncMiddleware from './utils/asyncMiddleware';
import flickrService from '../services/flickr';

const galleryRouter = router.Router();

galleryRouter.get('/all', asyncMiddleware(async (req, res) => {
  const images = await flickrService.search(req.query.page);
  res.send(images);
}));

galleryRouter.get('/info', asyncMiddleware(async (req, res) => {
  const imageInfo = await flickrService.findPhotoInfo(req.query.imageId);
  res.send(imageInfo);
}));


export default galleryRouter;
